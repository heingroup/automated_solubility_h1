from ftdi_serial import Serial
from north_devices.pumps.tecan_cavro import TecanCavro
from vicivalve.vicivalve import VICI
from hein_robots.robotics import Location, Cartesian
from hein_robots.grids import Grid
from hein_robots.kinova.kinova_gen3 import KinovaGen3Arm
from ika.magnetic_stirrer import MagneticStirrer
from hein_robots.kinova.kinova_sequence import KinovaSequence
from sampleomaticV2 import SampleomaticStepper
from mtbalance.mtbalance import ArduinoAugmentedQuantos


#cogrid_moves.pynstants
SYRINGE_VOLUME = 1  #mL
PUMP_RATE = 2 #ml/min
SAMPLE_PORT=3
STOCK_DRAW_RATE=2 #ml/min
# gripper commands based on gripper model
DEFAULT_G_CLOSE = 0.89
DEFAULT_G_CLOSE_CAPPING = 0.87
DEFAULT_G_CLOSE_UNCAPPED = 0.91
DEFAULT_G_OPEN = 0.75
DEFAULT_G_CLOSE_SM = 0.78
DEFAULT_G_OPEN_SM = 0.75


# location files
vision_station = KinovaSequence(r'C:\Users\User\PycharmProjects\Eve\tests\vision_location.json')
quantos_vial = KinovaSequence(r'C:\Users\User\PycharmProjects\Eve\tests\quantos_vial.json')
regripping_locations = KinovaSequence(r'C:\Users\User\PycharmProjects\Eve\tests\regripping_locations.json')
sm_locations = KinovaSequence(r'C:\Users\User\PycharmProjects\Eve\tests\sm_locations.json')
dosing_location = KinovaSequence(r'C:\Users\User\PycharmProjects\Eve\tests\dosing_location.json')
uncapping_location = KinovaSequence(r'C:\Users\User\PycharmProjects\Eve\tests\uncapping_location.json')


# modules
#robot_password=robot_password.password
arm = KinovaGen3Arm(default_velocity=400, password='admin')
mini_heater_stirrer = MagneticStirrer(device_port="COM14")
# dosing pump
serial_tecan = Serial(device_serial='FT2YOS81', baudrate=9600)
dosing_pump = TecanCavro(serial=serial_tecan, address=1, syringe_volume_ml=SYRINGE_VOLUME)
# selection valve
serial_vici = Serial(device_serial='FT0VORU5', baudrate=9600)
vici = VICI(serial=serial_vici,positions=10)
#HPLC tray locations
grid = Grid(Location(x=63.68, y=280, z=51.5, rx=0, ry=180, rz=90), rows=6, columns=4, spacing=Cartesian(-20.6, 20.6))
# sampleomatic
sm = SampleomaticStepper(serial_port=16)
quan = ArduinoAugmentedQuantos('192.168.254.2', 12, logging_level=10)
vision_station_camera_port = 0
gripper_type = 0
if gripper_type == 0:
    arm.tool_offset = Location(z=120)
elif gripper_type == 1:
    arm.tool_offset = Location(z=105)

# initialization
vici.home()
dosing_pump.home()

