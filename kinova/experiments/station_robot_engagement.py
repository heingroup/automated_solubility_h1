import time

from ..configuration import deck
from hein_robots.robotics import Location


vials = ['A1','B1','C1','D1',
         'A2','B2','C2','D2',
         'A3', 'B3', 'C3', 'D3',
         'A4', 'B4', 'C4', 'D4',
         'A5', 'B5', 'C5', 'D5',
         'A6', 'B6', 'C6', 'D6']


def reaction_grid_test():
    # gripper position to enter 20mm vial trays / open position
    deck.arm.close_gripper(0.81)
    for index in deck.grid.indexes:
        for i in range(2):
            location = deck.grid[index]
            safe_location = location.translate(z=100)
            print(index, location, safe_location)

            deck.arm.move_to_location(safe_location)

            deck.arm.move_to_location(location)
            time.sleep(2)

            if i == 0:
                # gripper position to close around HPLC vial / close position
                deck.arm.close_gripper(0.87)
            if i == 1:
                # gripper position to enter 20mm vial trays / open position
                deck.arm.close_gripper(0.81)

            deck.arm.move_to_location(safe_location)
            # time.sleep(1)


def reaction_tray(rxn_index : str = 'A1' , action: str = 'from', gripper_type: str = 'custom'):
    if action == 'from':
        deck.arm.close_gripper(0.81)

    location = deck.grid[rxn_index]
    safe_location = location.translate(z=100)

    deck.arm.move_to_location(safe_location)

    deck.arm.move_to_location(location)
    time.sleep(2)

    if action=='from':
        if gripper_type == 'custom':
            deck.arm.close_gripper(deck.CUSTOM_G_OPEN_SM)
        # gripper position to close around HPLC vial / close position
        deck.arm.close_gripper(0.87)
    if action=='to':
        # gripper position to enter 20mm vial trays / open position
        deck.arm.close_gripper(0.81)

    deck.arm.move_to_location(safe_location)
    # time.sleep(1)

def regrip_station(from_grip: str = 'vertical', to_grip: str = 'horizontal'):

    #vertical pose
    location_v=Location(x=28.59, y=600.7, z=178, rx=0, ry=180, rz=0)
    location_h=Location(x=25.89, y=565.9, z=146.9, rx=-90, ry=-180, rz=0)

    #approach
    if from_grip == 'vertical':
        safe_location = location_v.translate(z=50)
    if from_grip == 'horizontal':
        safe_location = location_h.translate(z=50)
    deck.arm.move_to_location(safe_location)

    #engage
    if from_grip == 'vertical':
        location = location_v
    if from_grip == 'horizontal':
        location = location_h
    deck.arm.move_to_location(location)

    # open gripper
    deck.arm.close_gripper(0.7)

    #withdraw
    if from_grip == 'vertical':
        safe_location = location_v.translate(z=50)
    if from_grip == 'horizontal':
        safe_location = location_h.translate(y=-30)
    deck.arm.move_to_location(safe_location)

    #rotate
    if to_grip == 'vertical':
        safe_location = (location_h + Location(y=-30,rx=90,ry=360))
    if to_grip == 'horizontal':
        safe_location = location_v + Location(z=50,rx=-90,ry=-360)
    deck.arm.move_to_location(safe_location)

    #approach
    if to_grip == 'vertical':
        safe_location = location_v.translate(z=50)
    if to_grip == 'horizontal':
        safe_location = location_h.translate(y=-30)
    deck.arm.move_to_location(safe_location)

    #engage
    if to_grip == 'vertical':
        location = location_v
    if to_grip == 'horizontal':
        location = location_h
    deck.arm.move_to_location(location)

    # close gripper
    deck.arm.close_gripper(0.87)

    #withdraw
    if to_grip == 'vertical':
        safe_location = location_v.translate(z=50)
    if to_grip == 'horizontal':
        safe_location = location_h.translate(z=50)
    deck.arm.move_to_location(safe_location)


def vision_station(action: str = 'from'):
    location=Location(x=-178.7, y=591.7, z=137.5, rx=0, ry=180, rz=90)
    safe_location = location.translate(z=60)
    face_location = location.translate(x=20, z=0.5)
    # safe height
    deck.arm.move_to_location(safe_location)
    if action=='from':
        #open gripper
        deck.arm.close_gripper(0.7)

    if action=='to':
        face_location = location.translate(x=20,z=0.5)
        deck.arm.move_to_location(face_location)

    # engage
    deck.arm.move_to_location(location)
    if action  == 'from':
        #close gripper
        deck.arm.close_gripper(0.87)
    if action == 'to':
        # open gripper
        deck.arm.close_gripper(0.7)

    # withdraw
    if action == 'from':
        location=location.translate(z=0.5)
        deck.arm.move_to_location(location)
        location=location.translate(x=20)
        deck.arm.move_to_location(location)
    deck.arm.move_to_location(safe_location)


# def vial_to_quantos():
#     """
#     picks up from the regripping stage and puts in quantos
#     :return:
#     """
#     # open gripper
#     deck.arm.close_gripper(0.81)
#     deck.arm.move_to_location(deck.quantos_locations['safe height'])
#     deck.arm.move_to_location(deck.quantos_locations['grip_horizontally'])
#     # close gripper
#     deck.arm.close_gripper(0.89)
#     deck.arm.move_to_location(deck.quantos_locations['safe height'])
#     deck.arm.move_to_location(deck.quantos_locations['retrieve'])
#     # OPEN QUANTOS DOOR HERE
#     deck.arm.move_to_location(deck.quantos_locations['lower_down'])
#     deck.arm.move_to_location(deck.quantos_locations['enter_quantos_safeheight'])
#     deck.arm.move_to_location(deck.quantos_locations['place_vial'])
#     # open gripper
#     deck.arm.close_gripper(0.81)
#     deck.arm.move_to_location(deck.quantos_locations['enter_quantos_safeheight'])
#     deck.arm.move_to_location(deck.quantos_locations['lower_down'])
#     deck.arm.move_to_location(deck.quantos_locations['retrieve'])

def vial_to_quantos():
    """
    move a vial from safe space to the quantos
    :return:
    """
    deck.arm.move_to_location(deck.vision_station['safe_space'])
    deck.quan.home_z_stage()
    deck.quan.front_door_position = "open"
    deck.arm.move_to_location(deck.quantos_vial['safe_height'],velocity=200)
    deck.arm.move_to_location(deck.quantos_vial['place_vial'],velocity=50)
    time.sleep(3)
    # gripper open
    deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
    deck.arm.move_to_location(deck.quantos_vial['safe_height'])
    deck.arm.move_to_location(deck.vision_station['safe_space'])
    deck.quan.front_door_position = "close"


def vial_from_quantos():
    deck.quan.home_z_stage()
    deck.quan.home_z_stage()
    deck.quan.front_door_position = "open"
    # gripper open
    deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
    deck.arm.move_to_location(deck.vision_station['safe_space'])
    deck.arm.move_to_location(deck.quantos_vial['safe_height'])
    deck.arm.move_to_location(deck.quantos_vial['grab_vial'])
    if deck.gripper_type == 0:
        deck.arm.close_gripper(deck.DEFAULT_G_CLOSE)
    elif deck.gripper_type == 1:
        deck.arm.close_gripper(deck.COSTUM_G_CLOSE_UNCAPPED)
    time.sleep(1.8)
    deck.arm.move_to_location(deck.quantos_vial['safe_height'])
    deck.arm.move_to_location(deck.vision_station['safe_space'])
    deck.quan.front_door_position = "close"



def vial_from_tray(rxn_index: str = 'A1'):
    # open gripper
    deck.arm.close_gripper(deck.CUSTOM_G_OPEN)
    location = deck.grid[rxn_index]
    safe_location = location.translate(z=310)
    deck.arm.move_to_location(safe_location)
    deck.arm.move_to_location(location)
    time.sleep(2)
    # close gripper
    deck.arm.close_gripper(deck.COSTUM_G_CLOSE)

    time.sleep(2)
    safe_location = location.translate(z=310)
    deck.arm.move_to_location(safe_location)


def vial_to_tray(rxn_index: str = 'A1', capped: bool=False):
    location = deck.grid[rxn_index]
    safe_location = location.translate(z=300)
    if capped:
        drop_location=location.translate(z=57)
    deck.arm.move_to_location(safe_location)
    if capped:
        deck.arm.move_to_location(drop_location)
    deck.arm.move_to_location(location)
    time.sleep(2)
    # open gripper
    deck.arm.close_gripper(0.81)
    # close gripper
    deck.arm.close_gripper(0.89)
    # open gripper
    deck.arm.close_gripper(0.81)
    deck.arm.move_to_location(safe_location)

def vial_to_regripping_station(direction: str = 'V', from_vision: bool = False):
    if direction == 'V':
        #away
        deck.arm.move_to_location(deck.regripping_locations["away_from_quantos_safeheight"])
        # safe height
        deck.arm.move_to_location(deck.regripping_locations['safeheight_v'])
        # place
        deck.arm.move_to_location(deck.regripping_locations['vision_station_v'])
        #open gripper
        deck.arm.open_gripper(0.7)
        # safe height
        deck.arm.move_to_location(deck.regripping_locations['safeheight_v'])
        #away
        deck.arm.move_to_location(deck.regripping_locations["away_from_quantos_safeheight"])
    if direction == 'H':
        deck.arm.move_to_location(deck.regripping_locations['safeheight_h'])
        # place
        deck.arm.move_to_location(deck.regripping_locations['vision_station_h'])
        # open gripper
        deck.arm.open_gripper(0.7)
        deck.arm.move_to_location(deck.regripping_locations['safeheight_h'])


def vial_from_regripping_station(direction: str = 'H'):
    if direction == 'H':
        deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
        deck.arm.move_to_location(deck.regripping_locations['safeheight_h'], velocity=500)
        # place
        deck.arm.move_to_location(deck.regripping_locations['vision_station_h'])
        # close gripper
        if deck.gripper_type == 0:
            deck.arm.close_gripper(deck.DEFAULT_G_CLOSE_UNCAPPED)
        elif deck.gripper_type == 1:
            deck.arm.close_gripper(deck.COSTUM_G_CLOSE_UNCAPPED)

        time.sleep(1.8)
        deck.arm.move_to_location(deck.regripping_locations['safeheight_h'])
    if direction == 'V':
        deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
        deck.arm.move_to_location(deck.regripping_locations['safeheight_v'])
        # place
        deck.arm.move_to_location(deck.regripping_locations['vision_station_v'])
        # close gripper
        if deck.gripper_type == 0:
            deck.arm.close_gripper(deck.DEFAULT_G_CLOSE_UNCAPPED)
        elif deck.gripper_type == 1:
            deck.arm.close_gripper(deck.COSTUM_G_CLOSE_UNCAPPED)
        time.sleep(1.8)
        # safe height
        deck.arm.move_to_location(deck.regripping_locations['safeheight_v'])
        # away
        deck.arm.move_to_location(deck.regripping_locations["away_from_quantos_safeheight"])


def uncap_vial():
    '''
    Assume holding a capped vial at safe space vertically
    ends up holding an uncapped vial at safe space vertically
    '''
    deck.arm.move_to_location(deck.uncapping_location['safe_height'])

    deck.arm.move_to_location(deck.uncapping_location['before_uncapping'],velocity=50)


    deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
    deck.arm.move(y=0,z=3,relative=True) #adjustment
    if deck.gripper_type == 0:
        deck.arm.close_gripper(deck.DEFAULT_G_CLOSE_CAPPING)
    elif deck.gripper_type == 1:

        deck.arm.close_gripper(deck.COSTUM_G_CLOSE)
        time.sleep(2)
        deck.arm.move(z=-5, relative=True)
    for i in range(10):
        deck.arm.move(x=0,y=0,z=0.5,rx=0, ry=0, rz=90, relative=True)
    print('done uncapping!')

    deck.arm.move_to_location(deck.uncapping_location['safe_height'])
    deck.arm.move_to_location(deck.uncapping_location['parking_above'])
    deck.arm.move_to_location(deck.uncapping_location['parking'], velocity=100)
    deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
    deck.arm.move_to_location(deck.uncapping_location['parking_above'])
    deck.arm.move_to_location(deck.uncapping_location['safe_height'])
    deck.arm.move_to_location(deck.uncapping_location['before_capping'],velocity=300)
    if deck.gripper_type ==0:
        deck.arm.move(y=1,z=-4,relative=True)
    if deck.gripper_type == 0:
        deck.arm.close_gripper(deck.DEFAULT_G_CLOSE_UNCAPPED)
    elif deck.gripper_type == 1:
        deck.arm.close_gripper(deck.COSTUM_G_CLOSE_UNCAPPED)
    time.sleep(2)
    deck.arm.move(z=200,relative=True,velocity=25)


def cap_vial():
    '''
    Assume holding an uncapped vial at safe space vertically
    ends up holding an uncapped vial at safe space vertically
    '''
    deck.arm.move_to_location(deck.uncapping_location['safe_height'], velocity=300)
    deck.arm.move_to_location(deck.uncapping_location['before_uncapping'], velocity=50)
    deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
    deck.arm.move_to_location(deck.uncapping_location['safe_height'], velocity=300)
    deck.arm.move_to_location(deck.uncapping_location['parking_above'])
    deck.arm.move_to_location(deck.uncapping_location['parking'], velocity=100)
    if deck.gripper_type == 0:
        deck.arm.close_gripper(deck.DEFAULT_G_CLOSE_CAPPING)
    elif deck.gripper_type == 1:
        deck.arm.close_gripper(deck.COSTUM_G_CLOSE)
        time.sleep(2)
    deck.arm.move_to_location(deck.uncapping_location['parking_above'])
    deck.arm.move_to_location(deck.uncapping_location['safe_height'], velocity=300)
    deck.arm.move_to_location(deck.uncapping_location['before_capping'], velocity=50)

    quarters = 9
    if deck.gripper_type == 0:
        deck.arm.close_gripper(deck.DEFAULT_G_CLOSE_CAPPING)
    elif deck.gripper_type == 1:
        deck.arm.move(z=-2,relative=True)

    for i in range(quarters):
        deck.arm.move(x=0,y=0,z=-0.5,rx=0, ry=0, rz=-90, relative=True)

    deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
    if deck.gripper_type == 0:
        deck.arm.close_gripper(deck.DEFAULT_G_CLOSE)
    elif deck.gripper_type == 1:
        deck.arm.close_gripper(deck.COSTUM_G_CLOSE)
    time.sleep(2)
    deck.arm.move_to_location(deck.uncapping_location['safe_height'])



def vial_to_vision(from_quantos: bool = False):
    deck.arm.move_to_location(deck.vision_station['safe_space'])
    deck.arm.move_to_location(deck.vision_station['safe_height'])
    if from_quantos:
        deck.arm.move_to_location(deck.vision_station['place_vial_from_quat'])
    else:
        deck.arm.move_to_location(deck.vision_station['place_vial'])
    # open gripper
    deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
    deck.arm.move_to_location(deck.vision_station['safe_height'])
    deck.arm.move_to_location(deck.vision_station['safe_space'])


def vial_from_vision():
    # open gripper
    deck.arm.move_to_location(deck.vision_station['safe_space'])
    deck.arm.close_gripper(deck.DEFAULT_G_OPEN)
    deck.arm.move_to_location(deck.vision_station['safe_height'])
    deck.arm.move_to_location(deck.vision_station['grab_vial'])
    # close gripper
    if deck.gripper_type == 0:
        deck.arm.close_gripper(deck.DEFAULT_G_CLOSE)
    elif deck.gripper_type == 1:
        deck.arm.close_gripper(deck.COSTUM_G_CLOSE)
    time.sleep(2)
    deck.arm.move_to_location(deck.vision_station['safe_height'])
    deck.arm.move_to_location(deck.vision_station['safe_space'])


def vial_to_regripping_station_to_horizontal():
    #away
    deck.arm.move_to_location(deck.regripping_locations["away_from_quantos_safeheight"])
    # safe height
    deck.arm.move_to_location(deck.regripping_locations['safeheight_v'])
    # place
    deck.arm.move_to_location(deck.regripping_locations['vision_station_v'])
    #open gripper
    deck.arm.open_gripper(0.7)
    # safe height
    deck.arm.move_to_location(deck.regripping_locations['safeheight_v'])
    #rotate
    deck.arm.move_to_location(deck.regripping_locations['safeheight_h'],velocity=500)
    # place
    deck.arm.move_to_location(deck.regripping_locations['vision_station_h'])
    #close gripper
    deck.arm.close_gripper(0.89)
    time.sleep(2)
    deck.arm.move_to_location(deck.regripping_locations['safeheight_h'])


def vial_to_regripping_station_to_vertical():
    deck.arm.move_to_location(deck.regripping_locations['safeheight_h'])
    # place
    deck.arm.move_to_location(deck.regripping_locations['vision_station_h'])
    #open gripper
    deck.arm.open_gripper(0.7)
    deck.arm.move_to_location(deck.regripping_locations['safeheight_h'])
    #rotate
    deck.arm.move_to_location(deck.regripping_locations['safeheight_v'])
    # place
    deck.arm.move_to_location(deck.regripping_locations['vision_station_v'])
    #close gripper
    deck.arm.close_gripper(0.89)
    time.sleep(2)
    # safe height
    deck.arm.move_to_location(deck.regripping_locations['safeheight_v'])
    #away
    deck.arm.move_to_location(deck.regripping_locations["away_from_quantos_safeheight"])



#def wash_dosing_line():
#
#def dose_solvent(stock_index: str = 'A1', to_vision: bool= True):
#    """ goes to stock, doeses solvent"""


def sm_pickup():
    deck.arm.move_to_location(deck.vision_station['safe_space'])
    # deck.arm.move_to_location(deck.sm['safe_space_high'])
    deck.arm.close_gripper(0.55)
    deck.arm.move_to_location(deck.sm_locations['lift_sm'])
    deck.arm.move_to_location(deck.sm_locations['grab_sm'])
    deck.arm.close_gripper(0.7)
    time.sleep(2)
    deck.arm.move_to_location(deck.sm_locations['lift_sm'])


def sm_park():
    deck.arm.move_to_location(deck.sm_locations['lift_sm'])
    deck.arm.move_to_location(deck.sm_locations['grab_sm'],velocity=50)

    time.sleep(2)
    deck.arm.close_gripper(0.55)
    time.sleep(0.5)
    deck.arm.close_gripper(0.7)
    time.sleep(2)
    deck.arm.move_to_location(deck.sm_locations['press_sm'])
    deck.arm.close_gripper(0.55)
    deck.arm.move_to_location(deck.sm_locations['lift_sm'])
    # deck.arm.move_to_location(deck.sm['safe_space_high'])
    deck.arm.move_to_location(deck.vision_station['safe_space'])



def sm_from_base_to_vision(vision_position: int = 1):
    """
    ends up at safe space
    :return:
    """
    deck.sm.home()
    deck.arm.move_to_location(deck.vision_station['safe_space'])
    # deck.arm.move_to_location(deck.sm['safe_space_high'])
    deck.arm.close_gripper(0.55)
    deck.arm.move_to_location(deck.sm_locations['lift_sm'])
    deck.arm.move_to_location(deck.sm_locations['grab_sm'])
    deck.arm.close_gripper(0.7)
    time.sleep(2)
    deck.arm.move_to_location(deck.sm_locations['lift_sm'])
    if vision_position == 1:
        deck.arm.move_to_location(deck.sm_locations['vision_1'])
    elif vision_position == 2:
        deck.arm.move_to_location(deck.sm_locations['vision_2'])

def sm_from_vision_to_base():
    """
    ends up at safe space
    :return:
    """
    deck.arm.move_to_location(deck.sm_locations['lift_sm'])
    deck.arm.move_to_location(deck.sm_locations['grab_sm'],velocity=50)

    time.sleep(2)
    deck.arm.close_gripper(0.55)
    time.sleep(0.5)
    deck.arm.close_gripper(0.7)
    time.sleep(2)
    deck.arm.move_to_location(deck.sm_locations['press_sm'])
    deck.arm.close_gripper(0.55)
    deck.arm.move_to_location(deck.sm_locations['lift_sm'])
    # deck.arm.move_to_location(deck.sm['safe_space_high'])
    deck.arm.move_to_location(deck.vision_station['safe_space'])


def dose_liquid(volume: float = 0, solvent : str = "water"):
    switcher = {
        'water' : 1,
        'ethanol': 2,
        'acetonitrile': 3,
        'acetone': 4,
        'propanol': 5,
        'ethyl acetate': 6
    }
    deck.sm.home()
    # lower sampleomatic
    deck.sm.move_needle(height= -35)
    # fill syringe with solvent A
    deck.vici.switch_valve(switcher.get(solvent,-1)) # BEAKER
    deck.dosing_pump.dispense_ml(volume, from_port=1, to_port=3, velocity_ml=deck.PUMP_RATE)
    #dip in
    deck.sm.move_needle(height= -15)
    # change pump port and push in sampleomatic
    deck.sm.home()


def zero_quantos():
    # tare
    deck.quan.front_door_position = "close"
    deck.quan.zero()


def weigh_and_dose_with_quantos(solid_weight: float = 1):
    zero_quantos()
    vial_to_regripping_station('H')
    vial_from_regripping_station('V')
    uncap_vial()
    vial_to_regripping_station('V')
    vial_from_regripping_station('H')
    vial_to_quantos()
    deck.quan.home_z_stage()
    # read weight
    deck.quan.front_door_position = "close"
    time.sleep(5)
    weight1 = float(deck.quan.weight_immediately)
    print("empty vial is", weight1)
    #deck.quan.front_door_position = "open"
    #dose
    deck.quan.home_z_stage()
    time.sleep(1)
    # z stage down
    deck.quan.move_z_stage(steps=8620)
    # close door
    deck.quan.front_door_position = "close"
    # dose
    deck.quan.target_mass = solid_weight
    deck.quan.start_dosing(wait_for=True)
    deck.quan.home_z_stage()
    time.sleep(5)
    dosed_val = float(deck.quan.weight_immediately)
    print(dosed_val)
    weight2 = weight1+dosed_val
    print("empty vial and solid mass is", weight2)
    deck.quan.front_door_position = "open"
    vial_from_quantos()
    vial_to_regripping_station('H')
    vial_from_regripping_station("V")
    cap_vial()
    vial_to_regripping_station("V")
    vial_from_regripping_station('H')

    return weight1, dosed_val, weight2


def weigh_with_quantos():
    """
    INVOLVES N9 MOVEMENTS: tares, takes vial in, weighs, takes vial out
    :return: weight in g
    """
    zero_quantos()
    vial_to_regripping_station('H')
    vial_from_regripping_station('V')
    uncap_vial()
    vial_to_regripping_station('V')
    vial_from_regripping_station('H')
    vial_to_quantos()
    deck.quan.home_z_stage()
    #read weight
    deck.quan.front_door_position = "close"
    time.sleep(5)
    weight = float(deck.quan.weight_immediately)
    deck.quan.front_door_position = "open"
    print("final weight", weight)
    vial_from_quantos()
    vial_to_regripping_station('H')
    vial_from_regripping_station("V")
    cap_vial()
    vial_to_regripping_station("V")
    vial_from_regripping_station('H')
    return weight  # in g


def dose_with_quantos(solid_weight: float = 0):
    # vial_to_regripping_station('H')
    # vial_from_regripping_station('V')
    # uncap_vial()
    # vial_to_regripping_station('V')
    # vial_from_regripping_station('H')
    vial_to_quantos()
    # deck.vision_gripper_vial_check()
    deck.quan.home_z_stage()
    time.sleep(1)
    # z stage down
    deck.quan.move_z_stage(steps=8700)
    # close door
    deck.quan.front_door_position = "close"
    # dose
    deck.quan.target_mass = solid_weight
    deck.quan.start_dosing(wait_for=True)
    time.sleep(5)
    x = float(deck.quan.weight_immediately)
    total_try_time = 100
    #for i in range (total_try_time):
    #    try:
    #        x = deck.quan.sample_data.quantity
    #        break
    #    except:
    #        print("cannot read sample mass, trying again (" +str(i)+'/'+str(total_try_time) +').' )
    print(x)
    mass_no_units = float(x)
    # open door
    deck.quan.front_door_position = "open"
    # z stage up
    deck.quan.move_z_stage(steps=-8000)
    deck.quan.home_z_stage()
    # take vial back to vision
    vial_from_quantos()
    # vial_to_regripping_station('H')
    # vial_from_regripping_station("V")
    # cap_vial()
    # vial_to_regripping_station("V")
    # vial_from_regripping_station('H')
    return mass_no_units



def dosing_head_from_holder_to_quan(position: int):
    deck.quan.unlock_dosing_pin_position()
    deck.arm.move_to_location(deck.dosing_location['in_' + str(position)])
    deck.arm.open_gripper(0.60)
    deck.arm.move_to_location(deck.dosing_location['grab_' + str(position)])
    deck.arm.open_gripper(0.78)
    time.sleep(2)
    deck.arm.move_to_location(deck.dosing_location['lift_' + str(position)])
    deck.arm.move_to_location(deck.dosing_location['out_' + str(position)])
    deck.arm.move_to_location(deck.dosing_location['safe_space'])
    deck.quan.home_z_stage()
    deck.quan.front_door_position = "open"
    deck.arm.move_to_location(deck.dosing_location['safe_inside_q'])
    deck.arm.move_to_location(deck.dosing_location['out_q'])
    deck.arm.move_to_location(deck.dosing_location['lift_q'])
    time.sleep(2)
    deck.arm.move_to_location(deck.dosing_location['grab_q'])
    deck.arm.move(x=-2,relative=True)
    deck.arm.open_gripper(0.6)
    deck.arm.move_to_location(deck.dosing_location['out_q'])
    deck.arm.move_to_location(deck.dosing_location['safe_inside_q'])
    deck.arm.move_to_location(deck.dosing_location['safe_space'])


def dosing_head_from_quan_to_holder(position : int):
    deck.quan.unlock_dosing_pin_position()
    deck.arm.move_to_location(deck.dosing_location['safe_space'])
    deck.quan.home_z_stage()
    deck.quan.front_door_position = "open"
    deck.arm.move_to_location(deck.dosing_location['safe_inside_q'])
    deck.arm.open_gripper(0.6)
    deck.arm.move_to_location(deck.dosing_location['in_q'])
    deck.arm.move_to_location(deck.dosing_location['grab_q'])
    deck.arm.open_gripper(0.78)
    time.sleep(2)
    deck.arm.move_to_location(deck.dosing_location['lift_q'])
    deck.arm.move_to_location(deck.dosing_location['out_q'])
    deck.arm.move_to_location(deck.dosing_location['safe_inside_q'])
    deck.arm.move_to_location(deck.dosing_location['safe_space'])
    deck.arm.move_to_location(deck.dosing_location['out_' + str(position)])
    deck.arm.move_to_location(deck.dosing_location['lift_' + str(position)])
    time.sleep(2)
    deck.arm.move_to_location(deck.dosing_location['grab_' + str(position)])
    deck.arm.open_gripper(0.6)
    deck.arm.move_to_location(deck.dosing_location['in_' + str(position)])


def dosing_head_from_2_to_1():
    deck.arm.move_to_location(deck.dosing_location['in_2'])
    deck.arm.open_gripper(0.60)
    deck.arm.move_to_location(deck.dosing_location['grab_2'])
    deck.arm.open_gripper(0.78)
    time.sleep(2)
    deck.arm.move_to_location(deck.dosing_location['lift_2'])
    deck.arm.move_to_location(deck.dosing_location['out_2'])
    deck.arm.move_to_location(deck.dosing_location['out_1'])
    deck.arm.move_to_location(deck.dosing_location['lift_1'])
    deck.arm.move_to_location(deck.dosing_location['grab_1'])
    deck.arm.open_gripper(0.6)
    deck.arm.move_to_location(deck.dosing_location['in_1'])
    deck.arm.move_to_location(deck.dosing_location['safe_space'])

    deck.quan.front_door_position = "close"




if __name__ == '__main__':
    deck.quan.home_z_stage()
    time.sleep(1)
    # z stage down
    deck.quan.move_z_stage(steps=8620)
    # close door
    deck.quan.front_door_position = "close"
    # dose
    deck.quan.target_mass = 7
    deck.quan.start_dosing(wait_for=True)
    deck.quan.home_z_stage()
    time.sleep(5)
    dosed_val = float(deck.quan.weight_immediately)
    print(dosed_val)
    # for i, vial in enumerate(vials):
    #vial_from_tray('A1')
    #uncap_vial()
    cap_vial()
    vial_to_tray('A1',capped=True)
    #weigh_and_dose_with_quantos(solid_weight=5)
    #weigh_with_quantos()
    # vial_from_regripping_station('V')
    # cap_vial()
    # vial_to_regripping_station('V')
    #ial_from_regripping_station('H')

    #ial_to_quantos()
    # dose_liquid()
    # vial_from_tray('D6')
    # vial_to_tray('D6')
    #vial_from_regripping_station('H')
    #vial_to_quantos()
    #vial_from_quantos()
    #vial_to_regripping_station(direction='H')
    #vial_from_regripping_station('H')
    #vial_to_vision()

    #dose_with_quantos(solid_weight=6)
    vial_from_tray('A1')
    vial_to_regripping_station("V")
    # vial_to_regripping_station_to_horizontal()
    # vial_to_quantos()
    # vial_from_quantos()
    # vial_to_vision()
    # vial_from_vision()
    #vial_to_regripping_station_to_horizontal()
    # vial_to_regripping_station_to_vertical()
    # vial_to_tray('A1')
   #for i in range(3):
   #    sm_from_base_to_vision()
   #    sm_from_vision_to_base()
    #reaction_tray()
    #reaction_grid_test()
    #vial_from_vision()
    # regrip_station(from_grip='vertical',to_grip='horizontal')
    #vision_station(action='to')
    #ial_to_regripping_station_to_vertical()
