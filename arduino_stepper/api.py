import serial
import logging
import time


class ArduinoStepper(object):
    """
    A class to communicate with an Arduino stepper motor controller.
    Serial commands
    SCR: Set comp register
    SPS: Set position in steps
    SIR: Set initial comp register value for acceleration
    QCP: Query for current position
    QTP: Query for target position
    QAP: Query for absolute position
    QCR: Query for the comp register value
    QIR: Query for initial comp register value for acceleration
    QRC: Query for default rms current
    STP: Stops the motor - returns current position and resets to zero
    SMS: Set the microstep. Default is 2
    HME: Rotates stepper motor until limit switch is pressed
    ZRO: Zeroes the stepTracker

    IDT: Turns on idle mode. Lowers current when motor is not rotating.
    IDF: Turns off idle mode
    SRC: Set the rms current
    SIC: Set the idle current

    Responses
    -1: good
    -2: error in command
    -3: busy
    -4: done action
    """
    def __init__(self,
                 motor_resolution,
                 gear_ratio,
                 com_port,
                 baudrate,
                 max_pulse_freq=4000,
                 interrupt=True,
                 timer_prescaler=64
                 ):
        self.motor_res = motor_resolution   # the motor resolution in step/rev when full stepping
        self._gear_ratio = gear_ratio        # driven/driving
        self.motorspeed: int = 200    # in steps/s (=counts/s)
        self._init_register: int = 1250     # the initial comp register value used for acceleration
        self._comp_register: int = 1250     # the default comp register value used for coasting speed
        self.default_ms = 2
        self._ms = self.default_ms
        self.microsteps = [2, 4, 8, 16, 32, 64, 128, 256]   # a list of microsteps that the driver can use
        self._comport = com_port
        self._baudrate = baudrate
        self._interrupt = interrupt
        self._timer_prescaler = timer_prescaler
        self._max_current = 1500    # mA
        self._idle_current = 0  # mA
        self._rms_current = 750     # mA
        if self._interrupt:
            self._max_pulse_freq = 1/(self._timer_prescaler * 0.0625 * 10**-6)  # 0.0625 from Arduino processor freq
        else:
            self._max_pulse_freq = max_pulse_freq  # the freq that an Arduino Uno can achieve using AccelStepper
        self._idle = False
        self.logger = logging.getLogger(f'{self.__class__.__name__}')
        self._connect()
        response = self._ser.read_until()
        self.logger.debug(f'"{response}"')

    def _connect(self):
        """Connect to the motor controller"""
        # serial connection
        self.logger.debug('initializing serial object')
        self._ser = serial.Serial(
            port=self.comport,
            baudrate=self.baudrate,
            parity=serial.PARITY_NONE,
            bytesize=serial.EIGHTBITS,
            stopbits=serial.STOPBITS_ONE,
        )
        # wait until "ready" is returned
        self.logger.debug('waiting for "ready"')
        self._ser.read_until()
        self.logger.debug('connected')

    @property
    def comport(self) -> str:
        return f'COM{self._comport}'

    @comport.setter
    def comport(self, value: int):
        self._comport = value

    @property
    def baudrate(self):
        return self._baudrate

    def send_string(self, string: str) -> str:
        """
        Sends the specified string on the serial IO

        :param string: sting to send
        :return: response
        """
        # append newline if not present
        self.logger.debug(f'sending "{string}"')
        if string.endswith('\r') is False:
            string = string + '\r'
        self._ser.write(string.encode())
        response = self._ser.read_until()
        self.logger.debug(f'received response: "{response}"')
        return response.decode()

    @property
    def interrupt(self) -> bool:
        return self._interrupt

    @property
    def timer_prescaler(self) -> int:
        return self._timer_prescaler

    @property
    def microstep(self) -> int:
        return self._ms

    @microstep.setter
    def microstep(self, microstep):
        if microstep not in self.microsteps:
            raise ValueError(f'The "{microstep}" is not in "{self.microsteps}".')
        else:
            cmd = 'SMS ' + str(microstep)
            self.send_string(string=cmd)
            self._ms = microstep

    @property
    def motor_resolution(self) -> int:
        return self.motor_res   # steps/rev

    @property
    def gear_ratio(self) -> float:
        return self._gear_ratio

    @property
    def max_current(self) -> int:
        return self._max_current

    @property
    def rms_current(self) -> int:
        return self._rms_current

    @rms_current.setter
    def rms_current(self, value: int):
        if value > self.max_current or value < 0:
            raise ValueError("Current is too high or negative.")
        else:
            self._rms_current = value
            cmd = 'SRC ' + str(value)
            self.send_string(cmd)

    @property
    def idle_current(self) -> int:
        return self._idle_current

    @idle_current.setter
    def idle_current(self, value: int):
        if value > self.rms_current or value < 0:
            raise ValueError("Idle current can't be larger than rms current or negative.")
        else:
            self._idle_current = value
            cmd = 'SIC ' + str(value)
            self.send_string(cmd)

    @property
    def idle_mode(self) -> bool:
        return self._idle

    @idle_mode.setter
    def idle_mode(self, state: bool):
        if state is True:
            self._idle = True
            self.send_string('IDT')
        else:
            self._idle = False
            self.send_string('IDF')

    @property
    def max_speed_steps(self) -> int:
        return int(round(self._max_pulse_freq / (self.microstep * self.gear_ratio)))

    @property
    def max_possible_speed_steps(self) -> int:
        return int(round(self._max_pulse_freq / (self.microsteps[0] * self.gear_ratio)))

    @property
    def max_speed_rev(self) -> float:
        return self._max_pulse_freq / (self.motor_resolution * self.microstep * self.gear_ratio)

    @property
    def max_possible_speed_rev(self) -> float:
        return self._max_pulse_freq / (self.motor_resolution * self.microsteps[0] * self.gear_ratio)

    @property
    def pulse_freq(self) -> int:
        return int(round(self.motorspeed * self.gear_ratio * self.microstep))

    @property
    def accel_comp_register(self) -> int:
        return self._init_register

    @accel_comp_register.setter
    def accel_comp_register(self, value: int):
        cmd = 'SIR ' + str(value)
        self.send_string(cmd)
        self._init_register = value

    @property
    def comp_register(self) -> int:
        return self._comp_register

    @comp_register.setter
    def comp_register(self, value):
        self._comp_register = value

    @property
    def motor_speed_steps(self) -> int:
        """The current motor speed in steps/s. After transformations by microstep and gear ratios."""
        return self.motorspeed

    @motor_speed_steps.setter
    def motor_speed_steps(self, motorspeed: int):
        target_speed_rev = motorspeed / self.motor_resolution
        if target_speed_rev > self.max_possible_speed_rev:
            raise ValueError(
                f'The "{motorspeed}" steps/s exceeds the maximum speed of "{self.max_possible_speed_steps}" steps/s.')
        elif target_speed_rev > self.max_speed_rev:
            i = 2
            for i in self.microsteps:
                if motorspeed * self.gear_ratio * i < self._max_pulse_freq:
                    self.microstep = i
                    self.motorspeed = motorspeed
                    break
        else:
            self.motorspeed = motorspeed

        if self.interrupt:
            period = (1/self.pulse_freq)*(10**6)
            self.comp_register = int(round(period/(0.0625*self.timer_prescaler)))
            cmd = 'SCR ' + str(self.comp_register)
        else:
            cmd = 'SCR ' + str(self.pulse_freq)
        self.send_string(string=cmd)

    @property
    def motor_speed_rev(self) -> float:
        """The motor speed in rev/s. After transformations by microstep and gear ratios"""
        return self.motor_speed_steps / self.motor_resolution

    @motor_speed_rev.setter
    def motor_speed_rev(self, motorspeed_rev: float):
        self.motor_speed_steps = motorspeed_rev * self.motor_resolution

    def rotate_steps(self, steps, speed: int = None, microstep: int = None, block: bool = False):
        """Rotates the motor to the desired number of steps and speed.

        :param steps: the number of steps to rotate
        :param speed: the speed of the motor in steps/s
        :param microstep: the microstep setting of the driver
        :param block: Makes the function blocking
        """
        if microstep is not None and microstep != self.microstep:
            self.microstep = microstep
        if speed is not None and speed != self.motor_speed_steps:
            self.motor_speed_steps = speed
        num_pulses = steps
        cmd = 'SPS ' + str(num_pulses)
        self.send_string(string=cmd)
        while block:
            time.sleep(1)
            reply = self._ser.read_until()
            reply = str(reply)
            if "-4" in reply:
                break

    def rotate_revolution(self, rev, speed: float = None, block: bool = False):
        """Rotates the motor to the desired revolutions at speed.

        :param rev: the number of revolutions to rotate
        :param speed: the speed in rev/s
        :param block: Makes the function blocking
        :return:
        """
        speed_steps = None
        if speed is not None:
            speed_steps = speed * self.motor_resolution
        steps = rev * self.motor_resolution * self.gear_ratio * self.microstep
        self.rotate_steps(steps=steps, speed=speed_steps, block=block)

    def home(self, direction: int, block: bool = True):
        """Rotates stepper motor until limit switch is pressed"""
        cmd = "HME " + str(direction)
        response = self.send_string(string=cmd)
        while block:
            time.sleep(1)
            reply = self._ser.read_until()
            reply = str(reply)
            if "-4" in reply:
                break
        return response

    def current_position(self):
        """Pings that Arduino for the current position of the motor while rotating."""
        current_position = self.send_string(string='QCP')
        return current_position

    def target_position(self):
        """Pings that Arduino for the target position of the motor while rotating."""
        target_position = self.send_string(string='QTP')
        return target_position

    def absolute_position(self):
        """Pings that Arduino for the absolute position of the motor"""
        abs_position = self.send_string(string='QAP')
        return abs_position

    def zero(self):
        """Zeros the step tracker of the motor"""
        response = self.send_string(string='ZRO')
        return response

    def busy(self):
        """Checks to see if the motor is busy."""
        response = self.send_string(string='BSY')
        return response

    def stop_motor(self):
        """Stops the motor"""
        response = self.send_string(string='STP')
        return response

    def ping_init_regsiter(self):
        response = self.send_string(string='QIR')
        self.accel_comp_register = int(response)
        return response

    def ping_comp_register(self):
        response = self.send_string(string='QCR')
        self.accel_comp_register = int(response)
        return response

    def ping_rms_current(self):
        response = self.send_string(string='QRC')
        self.rms_current = int(response)
        return response
