import time
import logging
from north_robots.components import Location
from n9.configuration import deck
logging.basicConfig(level=logging.WARN)


def position_and_gripper():
    while True:
        pos = deck.c9.joystick.record_position('Move the N9 to a position and press the OPTIONS button',
                                               use_probe=False)
        print(f'Position: x={pos[0]}, y={pos[1]}, z={pos[2]}, gripper={pos[3]}')


def position_and_position_difference():
    last_pos = None
    while True:
        pos_vector = deck.c9.joystick.record_position_vector('Move the N9 to a position and press the OPTIONS button',
                                                             use_probe=True)
        print(f'Position: {pos_vector}')

        if last_pos is not None:
            location = Location.location_from_origin_and_x_vector(pos_vector, last_pos)
            print(f'Location: {location}')

        last_pos = pos_vector


if __name__ == "__main__":
    position_and_gripper()


