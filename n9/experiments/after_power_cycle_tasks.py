from n9.configuration import deck
import logging
import time
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.info(f'Homing pumps...')
deck.sample_pump.home()
deck.dosing_pump.home()
deck.push_pump.home()
logger.warning(f'PRE-FLIGHT CHECKS: MAKE SURE GRIPPER IS CLEAN AND EMPTY')
time.sleep(0.5)
input('Press any key to continue and tare the scale')
deck.scale.tare()
print('done')

