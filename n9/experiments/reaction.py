import logging
import time
from n9.configuration import deck


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.WARN)


# stock solution concentrations
V_REACTION = 1
REACTION_VIAL_POSITIONS = ['A1', 'A2', 'A3', 'A4',
                           'B1', 'B2', 'B3', 'B4',
                           'C1', 'C2', 'C3', 'C4',
                           'D1', 'D2', 'D3', 'D4',
                           'E1', 'E2', 'E3', 'E4',
                           'F1', 'F2', 'F3', 'F4']


def gripper_empty_check():
    """
    this will check if gripper is empty prior to bringing anything to the gripper.
    The assumption is the empty scale has been tared when C9 was turned on (included in after_power_cycle_tasks.py)
    :return:
    """
    if deck.scale.weigh() > 0.05:
        action = input('something is on the gripper')


def priming():
    """
    flushes dosing pump
    :return:
    """
    deck.dosing_pump.dispense_ml(volume_ml=deck.DOSING_PUMP_VOL,
                                 from_port=deck.SOLVENT_PORT,
                                 to_port=deck.SAMPLE_PORT,
                                 velocity_ml=deck.SOLVENT_DRAW_RATE,
                                 dispense_velocity_ml=deck.SOLVENT_DRAW_RATE,
                                 )


def get_needle_with_vision() -> bool:
    deck.needle_tray.pickup()
    uncap_needle()
    needle_on_probe = deck.vision_needle_check()
    return needle_on_probe


def dump_needle_with_vision() -> bool:
    dump_needle()
    needle_on_probe = deck.vision_needle_check()
    return needle_on_probe


def uncap_needle():
    """
    set of arm movements to uncap the needle that is already on the probe
    :return:
    """
    # move to top of the bin, right height for uncapping Position
    deck.n9.move(x=158.2, y=-158.89, z=302.2, gripper=109.173, probe=True)
    # move to uncapping tooth Position
    deck.n9.move(x=128.83, y=-161.1, z=299.3, gripper=109.173, probe=True)
    # move up to uncap
    deck.n9.move(x=128.83, y=-161.1, z=303.3, gripper=109.173, probe=True)
    # back on the bin
    deck.n9.move(x=158.2, y=-158.89, z=302.2, gripper=109.173, probe=True)
    # close to camera detect cap
    move_for_needle_check()


def move_for_needle_check():
    deck.n9.move(x=328.7860595703125, y=59.38238525390625, z=297.7876708984375, probe=False)


def dump_needle():
    """
    set of arm movements to uncap the needle that is already on the probe
    :return:
    """
    # go to uncapping station and get rid of needle
    # move to top of the bin, right height for uncapping Position: (x=155.68, y=-162.7, z=305, gripper=45, probe=True)
    deck.n9.move(x=155.68, y=-161, z=307, gripper=45, probe=True)
    # # move to uncapping tooth Position: (x=128.4, y=-162.7, z=295, gripper=45, probe=True)
    deck.n9.move(x=155.68, y=-161, z=294, gripper=45, probe=True)

    deck.n9.move(x=127, y=-161, z=294, gripper=45, probe=True)
    # # move up to uncap:Position: (x=128.4, y=-162.7, z=305, gripper=45, probe=True)
    deck.n9.move(x=127, y=-161, z=307, gripper=45, probe=True)
    # # back on the bin
    deck.n9.move(x=127, y=-161, z=307, gripper=45, probe=True)
    move_for_needle_check()


def bring_stock_in_line(stock_index: str = 'A1', airgap_volume: float=0.05, sample_volume: float= 0):
    # go to stock solution
    location = deck.stock_grid.locations[stock_index]
    safe_location = location.copy(z=250)
    deck.n9.move_to_location(safe_location, gripper=0, probe=True)
    # change port to sample port and draws air gap
    deck.dosing_pump.switch_valve(deck.SAMPLE_PORT)
    deck.dosing_pump.move_absolute_ml(airgap_volume+0.15, deck.STOCK_DRAW_RATE)
    # go inside vial and draw liquid
    deck.n9.move_to_location(deck.stock_grid.locations[stock_index], gripper=0, probe=True)
    deck.dosing_pump.move_relative_ml(-0.15, deck.STOCK_DRAW_RATE)
    time.sleep(1)
    deck.dosing_pump.move_relative_ml(sample_volume, deck.STOCK_DRAW_RATE)
    # come out and up to safe height Position
    deck.n9.move_to_location(safe_location, gripper=0, probe=True)
    #draw equal amount of air
    deck.dosing_pump.move_relative_ml(0.05, deck.STOCK_DRAW_RATE)



def wash_dosing_line(by_tap_solvent: bool = True, wash_volume : float = 1):
    """
    this will use dosing pump to flush solvent through dosing line or discard what is currently in the line
    :param by_tap_solvent if true, washes the line by what is on tap
    :param wash_volume: wash volume by ml
    :return:
    """

    # to waste beaker at safe height
    deck.n9.move(x=105.4, y=276.6, z=300, gripper=2.9949999999999903, probe=True)
    # down in waste beaker
    deck.n9.move( x=105.4, y=276.6, z=157.7, gripper=2.9949999999999903, probe=True)
    if by_tap_solvent:
        # dosing pump draw and dispense
        deck.dosing_pump.dispense_ml(volume_ml=wash_volume,
                                     from_port=deck.SOLVENT_PORT,
                                     to_port=deck.SAMPLE_PORT,
                                     velocity_ml=deck.SOLVENT_DRAW_RATE,
                                     dispense_velocity_ml=deck.SOLVENT_DRAW_RATE,
                                     )
    else:
        # dosing dispense what is already in the line
        deck.dosing_pump.switch_valve(deck.SAMPLE_PORT)
        deck.dosing_pump.move_absolute_ml(0, velocity_ml=deck.SOLVENT_DRAW_RATE)

    # bring arm up to safe height
    deck.n9.move(x=105.4, y=276.6, z=300, gripper=2.9949999999999903, probe=True)

if __name__ == '__main__':
    while True:
        deck.needle_tray.pickup()
        dump_needle()



