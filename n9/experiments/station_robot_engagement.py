import logging
import time
from north_robots.components import Location
from n9.configuration import deck
from n9.experiments.capping_uncapping import recap,uncap
logging.basicConfig(level=logging.WARN)


def zero_quantos():
    # tare
    deck.quan.front_door_position = "close"
    deck.quan.zero()


def weigh_with_quantos():
    """
    INVOLVES N9 MOVEMENTS: tares, takes vial in, weighs, takes vial out
    :return: weight in g

    """
    zero_quantos()
    vial_to_quantos()
    deck.quan.home_z_stage()
    #read weight
    deck.quan.front_door_position = "close"
    time.sleep(5)
    weight = float(deck.quan.weight_immediately)
    deck.quan.front_door_position = "open"

    vial_from_quantos()
    return weight # in g


def dose_with_quantos(solid_weight: float = 0):
    vial_to_quantos()
    location = deck.vision_grid.locations['A1']
    face_hole_location = Location(x=location.x, y=200, z=location.z)
    safe_location = Location(x=face_hole_location.x, y=face_hole_location.y, z=300)
    deck.n9.move_to_location(safe_location, probe=False, gripper=0)
    # deck.vision_gripper_vial_check()
    deck.quan.home_z_stage()
    time.sleep(1)
    # z stage down
    deck.quan.move_z_stage(steps=8700)

    # close door
    deck.quan.front_door_position = "close"

    # dose
    deck.quan.target_mass = solid_weight
    deck.quan.start_dosing(wait_for=True)
    x = deck.quan.sample_data.quantity
    print(x)
    mass_no_units = float(x)

    # open door
    deck.quan.front_door_position = "open"

    # z stage up
    deck.quan.move_z_stage(steps=-8000)

    deck.quan.home_z_stage()

    # take vial back to vision
    vial_from_quantos()
    return  mass_no_units


def poke_vision_top_open():
    # safe height
    deck.n9.move( x=122.38, y=111.45, z=300, gripper=91.286)
    # down ready to open
    deck.n9.move( x=122.38, y=111.45, z=139.7, gripper=91.286)
    # poke open
    deck.n9.move(x=122.38, y=98.9, z=139.7, gripper=91.286)
    # down ready to open
    deck.n9.move( x=122.38, y=111.45, z=139.7, gripper=91.286)
    # safe height
    deck.n9.move(x=122.38, y=111.45, z=300, gripper=91.286)


def poke_vision_top_close():
    deck.c9.output(0, False)
    # go to safe height
    deck.n9.move(x=125.9, y=33, z=300, gripper=91.23325)
    # down ready to close
    deck.n9.move(x=125.9, y=33, z=138, gripper=91.23325)
    # poke close
    deck.n9.move(x=125.9, y=58, z=138, gripper=91.23325)
    # down ready to close
    deck.n9.move(x=125.9, y=33, z=138, gripper=91.23325)
    # safe height
    deck.n9.move(x=125.9, y=33, z=300, gripper=91.23325)


def park_cap():
    # park cap
    location = deck.CAP_PARKING
    safe_location = Location(location.x,location.y,310)
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)
    deck.n9.move_to_location(location, gripper=150.6, probe=False)
    time.sleep(1)
    recap(controller=deck.c9, pitch_mm=2, rotations=1)
    time.sleep(1)
    # park cap
    deck.c9.output(0, False)
    # up at safe height
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)


def pickup_cap():
    location = deck.CAP_PARKING
    safe_location = Location(location.x,location.y,310)
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)
    deck.n9.move_to_location(location, gripper=150.6, probe=False)
    # park cap
    deck.c9.output(0, True)
    # up at safe height
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)


def take_arm_out_of_quantos():
    # go up on the vial
    deck.n9.move(x=-225.2, y=-188.3, z=156.6, gripper=52.613)
    # go to safe middle
    deck.n9.move(x=-225.2, y=41.04, z=156.6, gripper=52.61)
    # go up on that location to safe height
    deck.n9.move(x=-225.2, y=41.04, z=300, gripper=52.61)


def vial_from_quantos():
    #open quantos door first
    deck.quan.front_door_position="open"
    deck.quan.home_z_stage()
    # go to safe middle location at safe height
    deck.n9.move(x=-225.2, y=41.04, z=300, gripper=52.61)
    # go to middle safe location lower
    deck.n9.move(x=-225.2, y=41.04, z=156.6, gripper=52.61)
    # go inside quntos and above
    deck.n9.move(x=-225.2, y=-188.3, z=156.6, gripper=52.613)
    # go down and grab vial
    deck.n9.move(-225.2, y=-188.3, z=137.5, gripper=52.613)
    deck.c9.output(0,True)
    # go up
    deck.n9.move(x=-225.2, y=-188.3, z=156.6, gripper=52.613)
    # go to safe middle
    deck.n9.move(x=-225.2, y=41.04, z=156.6, gripper=52.61)
    # go up on that location to safe height
    deck.n9.move(x=-225.2, y=41.04, z=300, gripper=52.61)
    deck.quan.front_door_position = "close"



def vial_to_quantos():
    # go to safe middle at safe height
    deck.n9.move(x=-225.2, y=41.04, z=300, gripper=52.61)
    # drive z stage up (home)
    deck.quan.home_z_stage()
    # open door
    deck.quan.front_door_position="open"
    # go to middle safe location lower
    deck.n9.move(x=-225.2, y=41.04, z=156.6, gripper=52.61)
    # go inside quntos and above
    deck.n9.move(x=-225.2, y=-188.3, z=156.6, gripper=52.613)
    # go inside quantos and inside vial holder z 145
    deck.n9.move(x=-225.2, y=-188.3, z=143, gripper=52.613)
    time.sleep(1)
    # ungrip vial
    deck.c9.output(0, False)
    # go up
    deck.n9.move(x=-225.2, y=-188.3, z=156.6, gripper=52.613)
    # go to safe middle
    deck.n9.move(x=-225.2, y=41.04, z=156.6, gripper=52.61)
    # go up on that location to safe height
    deck.n9.move(x=-225.2, y=41.04, z=300, gripper=52.61)


def vial_from_tray(rxn_index: str = 'A1', capped: bool=False):
    """
    """
    # arm gripper should be open
    deck.c9.output(0, False)
    location = deck.reaction_grid_gripper.locations[rxn_index]
    safe_location = location.copy(z=300)
    # deck.n9.gripper.move_degrees(180)
    deck.n9.move_to_location(safe_location, probe=False)
    if capped:
        grip_cap_location = location.copy(z=169.7)
    else:
        grip_cap_location = location.copy(z=163)
    deck.n9.move_to_location(grip_cap_location)
    deck.c9.output(0, True)
    deck.n9.move_to_location(safe_location, probe=False)


def vial_to_tray(rxn_index: str = 'A1', capped: bool=False):
    location = deck.reaction_grid_gripper.locations[rxn_index]
    safe_location = location.copy(z=300)
    # deck.n9.gripper.move_degrees(180)
    deck.n9.move_to_location(safe_location, probe=False)
    if capped:
        grip_cap_location = location.copy(z=169.7)
    else:
        grip_cap_location = location.copy(z=163)
    deck.n9.move_to_location(grip_cap_location)
    deck.c9.output(0, False)
    deck.n9.move_to_location(safe_location, probe=False)


def vial_to_vision(vision_index: str = 'A1', drop_times: int = 0, capped: bool=False):
    location = deck.vision_grid.locations[vision_index]
    face_hole_location = Location(x=location.x, y=200, z=location.z)
    safe_location = Location(x=face_hole_location.x, y=face_hole_location.y, z=300)
    deck.n9.move_to_location(safe_location, probe=False, gripper=0)
    deck.n9.move_to_location(face_hole_location, probe=False, gripper=0)
    deck.n9.move_to_location(location, probe=False, gripper=0)
    deck.c9.output(0, False)
    time.sleep(1)
    deck.n9.move_to_location(face_hole_location, probe=False, gripper=0)
    deck.n9.move_to_location(safe_location, probe=False, gripper=0)


def vial_from_vision(vision_index: str = 'A1', capped: bool = False):
    # arm gripper should be open
    deck.c9.output(0, False)
    # open the vision door
    #poke_vision_top_open()
    location = deck.vision_grid.locations[vision_index]
    face_hole_location = Location(x=location.x ,y=200, z=location.z)
    safe_location = Location(x=face_hole_location.x, y=face_hole_location.y, z=300)
    grip_cap_location = Location(x=location.x, y=location.y, z=146.2)
    deck.n9.move_to_location(safe_location, probe=False, gripper=0)
    deck.n9.move_to_location(face_hole_location, probe=False, gripper=0)
    deck.n9.move_to_location(location, probe=False, gripper=0)
    deck.n9.move_to_location(grip_cap_location, probe=False, gripper=0)
    deck.c9.output(0, True)
    deck.n9.move_to_location(location, probe=False, gripper=0)
    deck.n9.move_to_location(face_hole_location, probe=False, gripper=0)
    deck.n9.move_to_location(safe_location, probe=False, gripper=0)
   # safe_location = location.copy(z=300)
   # deck.n9.move_to_location(safe_location, probe=False)
   # if capped:
   #     grip_cap_location = location.copy(z=145.5)
   # else:
    #    grip_cap_location = location.copy(z=132.2)
   # deck.n9.move_to_location(grip_cap_location)
    #deck.c9.output(0, True)
    #deck.n9.move_to_location(safe_location, probe=False)


def vial_to_gripper(rxn_index: str ='A1', capped: bool= False, from_tray: bool = False):
    if from_tray:
        # check if gripper empty
        deck.c9.output(1, False)
        if deck.scale.settled_weigh(matching=2) > 0.05:
            action= input(print("GRIPPER NOT EMPTY"))
        #have arm gripper open
        deck.c9.output(0,False)
        location = deck.reaction_grid_gripper.locations[rxn_index]
        safe_location = location.copy(z=300)
        deck.n9.move_to_location(safe_location, gripper=109)
        if capped:
            grip_cap_location = location.copy(z=169.7)
        else:
            grip_cap_location = location.copy(z=162.9)
        deck.n9.move_to_location(grip_cap_location, gripper=109)
        # # gripper closes around vial's cap
        deck.c9.output(0, True)
        # # bring it to safe height Position: x=-193.2, y=260.3, z=186.005, gripper=150.61699999999996
        deck.n9.move_to_location(safe_location, gripper=109)
    # go to scale still on safe height Position: x=-186.3, y=-16.3, z=186.005, gripper=149.60299999999995
    deck.n9.move(x=-186.6, y=-13.9, z=300, gripper=150.62)
    if capped:
        # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=120.05, gripper=150.62)
    else:
        # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=111.6, gripper=150.62)
    # scale gripper closes around vial
    deck.c9.output(1, True)
    # arm gripper opens around the cap
    deck.c9.output(0, False)
    # gripper goes up to safe height
    deck.n9.move(x=-186.6, y=-13.9, z=300, gripper=150.62099999999998)


def vial_from_gripper(rxn_index='A1', capped: bool= False, to_tray: bool= False):
    deck.c9.output(0, False)
    # todo add gripper location to deck
    # go to scale still on safe height Position: x=-186.3, y=-16.3, z=186.005, gripper=149.60299999999995
    deck.n9.move(x=-186.6, y=-13.9, z=300, gripper=150.62)
    if capped:
        # # # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=120.05, gripper=150.62)
    else:
        # # # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=108.68, gripper=150.62)

    # arm gripper closes around the cap
    deck.c9.output(0, True)
    # scale gripper open around vial
    deck.c9.output(1, False)
    time.sleep(6)
    # gripper goes up to safe height
    deck.n9.move(x=-186.6, y=-13.9, z=250, gripper=150.62099999999998)
    if to_tray:
        location = deck.reaction_grid_gripper.locations[rxn_index]
        safe_location = location.copy(z=300)
        deck.n9.move_to_location(safe_location, gripper=150)
        if capped:
            grip_cap_location = location.copy(z=169.7)
        else:
            grip_cap_location = location.copy(z=163)
        deck.n9.move_to_location(grip_cap_location, gripper=150)
        # # gripper opens around vial's cap
        deck.c9.output(0, False)
        # # bring it to safe height Position: x=-193.2, y=260.3, z=186.005, gripper=150.61699999999996
        deck.n9.move_to_location(safe_location, gripper=150)


if __name__ == '__main__':
    vial_from_gripper('A3')