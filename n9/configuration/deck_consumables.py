from n9.configuration import deck
from north_robots.components import GridTray, Location
import pandas as pd
import time

# important paths to files
stock_info_path = r'C:\git_repositories\smaug\configuration\stock_info.csv'
vial_info_path = r'C:\git_repositories\smaug\configuration\vial_info.csv'
stocks_df = pd.read_csv(stock_info_path)
vials_df = pd.read_csv(vial_info_path)


def read_stocks_file():
    global stocks_df
    stocks_df = pd.read_csv(stock_info_path)


class Chemical:
    def __init__(self,
                 name: str,
                 density: float = None,  # g/mL
                 formula: str = None
                 ):
        self.name = name
        self.density = density
        self.formula = formula


class Container:
    def __init__(self,
                 max_volume: float = None,
                 safe_volume: float = None,
                 current_volume: float = None,
                 ):
        self.max_volume = max_volume
        self.safe_volume = safe_volume
        self.current_volume = current_volume


class DeckContainer(Container):
    def __init__(self,
                 tray: GridTray,
                 index: str,
                 capped: bool = False,
                 max_volume: float = None,
                 safe_volume: float = None,
                 current_volume: float = None,
                 ):
        """
        Some kind of container on the deck
        :param tray:
        :param index:
        :param capped:
        """
        super().__init__(max_volume=max_volume,
                         safe_volume=safe_volume,
                         current_volume=current_volume,
                         )
        self.tray: GridTray = tray
        self.index: str = index
        self.capped: bool = capped


class ChemicalContainer(Chemical, DeckContainer):
    def __init__(self,
                 name: str,
                 density: float,  # g/mL
                 tray: GridTray,
                 index: str,
                 capped: bool = False,
                 max_volume: float = None,
                 safe_volume: float = None,
                 current_volume: float = None,
                 ):
        Chemical.__init__(self,
                          name=name,
                          density=density)
        DeckContainer.__init__(self,
                               tray=tray,
                               index=index,
                               capped=capped,
                               max_volume=max_volume,
                               safe_volume=safe_volume,
                               current_volume=current_volume,
                               )


# function to organize vials on the deck
def get_clean_vial(with_stirbar: bool = True):
    # find which vial to use
    vials = pd.read_csv(vial_info_path)
    #print(vials)
    # find first clean vial row
    vial = vials[(vials['used'] == False) & (vials['stirbar'] == with_stirbar)].first_valid_index()# row index in dataframe
    #print(vial)
    if vial is None:
        # no clean vials
        return None, None
    # read the index and capped status
    vial_index = vials.loc[vial, 'vial_index']
    vial_cap_status = vials.loc[vial, 'capped']
    vials.loc[vial, 'used'] = True
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            vials.to_csv(vial_info_path, index=False)
            break
        except PermissionError as e:
            time.sleep(1)
    return vial_index, vial_cap_status


def get_stock_info(name: str = 'acetone'):
    """
    Example use:
        density, tray, index, capped = deck_consumables.get_stock_info(solvent_name)

    :param name:
    :return:
    """
    # find which vial to use
    stocks = pd.read_csv(stock_info_path)
    # print row which contains that name
    df= stocks[stocks['name'].str.match(name)]
    print(df)
    #return its info
    print(df.loc[:,'density'].iloc[0], df.loc[:,'tray'].iloc[0], df.loc[:,'index'].iloc[0], df.loc[:,'capped'].iloc[0])
    return df.loc[:,'density'].iloc[0], df.loc[:,'tray'].iloc[0], df.loc[:,'index'].iloc[0], df.loc[:,'capped'].iloc[0]


def get_stock(name: str) -> ChemicalContainer:
    """
    Return a ChemicalContainer for a chemical using information from the stock csv file
    :param name: name of chemical
    :return:
    """
    read_stocks_file()
    info: pd.DataFrame = stocks_df[stocks_df.name == name]  # all the info for the chemical
    name = info['name'].values[0]
    density = info['density'].values[0]
    tray = f"{info['tray'].values[0]}"
    index = info['index'].values[0]
    capped = info['capped'].values[0]
    max_volume = info['max volume'].values[0]
    safe_volume = info['safe volume'].values[0]
    current_volume = info['current volume'].values[0]
    if tray == 'deck.stock_grid':
        tray = deck.stock_grid
    stock = ChemicalContainer(name=name,
                              density=density,
                              tray=tray,
                              index=index,
                              capped=capped,
                              max_volume=max_volume,
                              safe_volume=safe_volume,
                              current_volume=current_volume,
                              )
    return stock


def update_stock_file(name: str,
                      column: str,
                      value):
    """

    :param str, name: stock name
    :param str, column: column heading
    :param value: new value
    :return:
    """
    read_stocks_file()
    info: pd.DataFrame = stocks_df[stocks_df.name == name]  # all the info for the chemical
    row_index = info.index[0]
    stocks_df.at[row_index, column] = value
    try:
        stocks_df.to_csv(stock_info_path, sep=',', index=False, mode='w')
        time.sleep(1)
    except PermissionError as e:
        pass


if __name__ == '__main__':
    get_stock_info()