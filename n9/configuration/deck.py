import time
import datetime
import pandas as pd
from copy import copy
from north_devices.pumps.tecan_cavro import TecanCavro
from north_c9.controller import C9Controller
from north_robots.components import GridTray
from north_robots.n9 import N9Robot
from ika.magnetic_stirrer import MagneticStirrer
from n9.components.managers import NeedleTrayManager
from north_devices.scales.scientech_zeta import ScientechZeta
from north_robots.components import Location
from pathlib import Path
from mtbalance import ArduinoAugmentedQuantos
import cv2
from heinsight.vision_utilities.camera import Camera
from heinsight.vision_utilities.colour_matcher import HSVMatcher
from hein_utilities.datetime_utilities import datetimeManager

# system specific constants
# switching sample loop on valves
SAMPLE_INLINE = "A"
SAMPLE_BYPASS = "B"
HPLC_FILL = "A"
HPLC_INJECT = "B"
LOOP_A_INJECT = "A"
LOOP_A_LOAD = "B"
LOOP_B_INJECT = "B"
LOOP_B_LOAD = "A"

# ports on the Cavro pumps
SAMPLE_PORT = 1
SOLVENT_PORT = 2

# syringe volumes
SAMPLE_PUMP_VOL = 2.5  # mL
PUSH_PUMP_VOL = 2.5  # mL
DOSING_PUMP_VOL = 1  # mL
AIR_VOLUME = 0.273  # ml air needed to transfer sample from sampleomatic head to sample loop
# flow rates
STOCK_DRAW_RATE = 2
SAMPLE_DRAW_RATE = 0.5  # mL/min
SOLVENT_DRAW_RATE = 5  # mL/min
SOLVENT_PUSH_RATE = 1  # mL/min
SOLVENT_DRAW_RATE_PPUMP = 5  # mL/min push pump
SOLVENT_PUSH_RATE_PPUMP = 0.42  # mL/min  push pump
SAMPLE_FLUSH_RATE = 3  # mL/min

# flush volumes
SAMPLE_FLUSH_VOLUME = SAMPLE_PUMP_VOL  # mL
PUSH_FLUSH_VOLUME = PUSH_PUMP_VOL  # mL

c9 = C9Controller(device_serial='FT2FT5C1', use_joystick=False)
n9 = N9Robot(c9)
n9.home()
# read gripper base current average
sum = 0
for i in range(10):
    sum += c9.axis_current(0)
ave=sum/10
print("gripper base current is", ave)
with open('gripper_base_current_data.csv', 'a+') as f:
    f.write(f'{ave}\n')

scale = ScientechZeta(c9.com(1, baudrate=19200)) # baudrate = 19200, 9600

serial_cavro = c9.com(0, baudrate=9600)  # Cavro pumps default to 9600
dosing_pump = TecanCavro(serial_cavro, address=1, syringe_volume_ml=DOSING_PUMP_VOL)

# IKA heater
heater_stirrer_port = 'COM16'
heater_stirrer= MagneticStirrer(device_port=heater_stirrer_port)

# mini Ika
mini_heater_stirrer_port = 'COM17'
mini_heater_stirrer= MagneticStirrer(device_port=mini_heater_stirrer_port)

# Quantos
quan = ArduinoAugmentedQuantos('192.168.254.30:8004', 13, logging_level=10)



needle_tray_state_file_location = str(Path(r'C:\git_repositories\smaug\state\needle_tray.json'))
# Using NeedleTrayManager class to define and array of needle trays, then picking them up one by one by pickup method
needle_tray = NeedleTrayManager(n9, trays=[
    GridTray(Location(x=297.1, y=147.2, z=159.4, rz=-89.45), rows=2, columns=24, spacing=(8.9, 8.8)),
    GridTray(Location(x=326.2, y=147.0, z=185.4, rz=-89.45), rows=2, columns=24, spacing=(8.9, 8.8)),
], probe=True, state_file=needle_tray_state_file_location)

# single locations
#gripper
BALANCE_GRIPPER = Location(x=-186.6, y=-13.9, z=116.05)
CAP_PARKING = Location(x=-319, y=-13.9, z=186)

#grids
stock_grid = GridTray(Location(136.6, -1.95, 140, rz=-89), rows=5, columns=3, spacing=(27.5, 25.5))
reaction_grid_gripper = GridTray(Location(x=-291.5, y=170.1, z=169.75, rz=-89.5), rows=4, columns=6, spacing=(20.5, 20))
vision_grid = GridTray(Location(x=212.4, y=184.4, z=147, rz=-90.9), rows=2, columns=1, spacing=(30,30))
vision_grid_needle = GridTray(Location(x=209.5, y=185.2, z=196.7, rz=-90.5), rows=2, columns=1, spacing=(30,30))
vision_grid_sampleomatic = GridTray(Location(x=183.2, y=185.5, z=263.2, rz=-91), rows=2, columns=1, spacing=(30,30))

vision_station_camera_port = 1
deck_camera_port = 0


# checking which camera port is which
# display(Camera(port=deck_camera_port).take_photo())
# display(Camera(port=vision_station_camera_port).take_photo())

deck_camera_images_folder = Path(r'C:\git_repositories\smaug\configuration\deck camera\ deck camera')
deck_camera = Camera(port=deck_camera_port,
                     save_folder=deck_camera_images_folder,
                     datetime_string_format='%Y_%m_%d_%H_%M_%S',
                     )


# vision error handling
vision_config_folder = Path(r'C:\git_repositories\smaug\configuration\vision')
vision_error_check_path = vision_config_folder.joinpath('vision needle check.json')
needle_roi = 'needle'
vision_check = HSVMatcher()
vision_check.load_data(str(vision_error_check_path))

annotation_width = 10
annotation_height = 25
green = (0, 255, 0)
annotation_font_scale = 0.55
annotation_thickness = 2
datetime_formatter = datetimeManager()


def vision_needle_check(image=None) -> bool:
    if image is None:
        image = deck_camera.take_photo(save_photo=False)
    clone = copy(image)
    needle_on_probe: bool = vision_check.good(clone,
                                              roi_name=needle_roi,
                                              channel='h')
    now = datetime_formatter.now_string()
    cv2.putText(clone,
                f'needle on probe check: {needle_on_probe}',
                (annotation_width, annotation_height),
                cv2.FONT_HERSHEY_SIMPLEX,
                annotation_font_scale,
                green,
                annotation_thickness)
    image_name = f'needle_check_{needle_on_probe}_{now}.png'
    path_to_save_image = str(deck_camera_images_folder.joinpath(image_name))
    cv2.imwrite(path_to_save_image, clone)

    print(f'needle on probe: {needle_on_probe}')
    return needle_on_probe

def position_record():
    axis_position_info = r'C:\git_repositories\smaug\configuration\axis_position_info.csv'
    df = pd.read_csv(axis_position_info)
    dic_row={'1':c9.axis_position(1), "1motor":c9.axis_position(1,motor=True),
             "1difference": abs(c9.axis_position(1) - c9.axis_position(1, motor=True)),
             "2":c9.axis_position(2), "2motor":c9.axis_position(2,motor=True),
             "2difference":abs(c9.axis_position(2) - c9.axis_position(2, motor=True)),
             "3":c9.axis_position(3), "3motor": c9.axis_position(3, motor=True),
             "3difference": abs(c9.axis_position(3) - c9.axis_position(3, motor=True)),"timestamp": datetime.datetime.now()}
    df=df.append(dic_row,ignore_index=True)
    if abs(c9.axis_position(2) - c9.axis_position(2, motor=True))>162:
        c9.home(if_needed=False)
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            df.to_csv(axis_position_info, index=False)
            break
        except PermissionError as e:
            time.sleep(1)


